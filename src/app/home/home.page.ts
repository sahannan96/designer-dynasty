import { Component, OnInit } from '@angular/core';
import { CrudService } from '../services/crud/crud.service';
import { AuthService } from '../services/auth/auth.service';
import { NavigationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  spinner: boolean = true;
  customerCount: number;
  purchaseInvoiceCount: number;
  itemCount: number;
  counts: any;
  constructor(
    private readonly crudService: CrudService,
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map((event) => {
          this.crudService.countDocs('customers').then((res) => {
            this.customerCount = res;
          });
          this.crudService.countDocs('purchase_invoice').then((res) => {
            this.purchaseInvoiceCount = res;
          });
          this.crudService.countDocs('items').then((res) => {
            this.itemCount = res;
            this.spinner = false
          });
          return event;
        })
      )
      .subscribe();
  }

  logout() {
    this.authService.signOut().then(() => {
      this.router.navigateByUrl('/', { replaceUrl: true });
    });
  }
}
