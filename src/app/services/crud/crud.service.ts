import { Injectable } from '@angular/core';
import { SupabaseClient, createClient } from '@supabase/supabase-js';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CrudService {
  private supabase: SupabaseClient;
  constructor() {
    this.supabase = createClient(
      environment.supabaseUrl,
      environment.supabaseKey
    );
  }

  async create(collection: string, data: any) {
    return await this.supabase.from(collection).insert(data).select().single();
  }

  async list(
    collection: string,
    fields: string = '*',
    match: any = '',
    from_range: number = 0,
    to_range: number = 10,
    sort: string = ''
  ) {
    let params = '';
    if (match) {
      Object.keys(match).forEach((key) => {
        console.log(key);
        console.log(match[key]);
        match[key] = match[key];
      });
    }
    const res = await this.supabase
      .from(collection)
      .select(fields)
      // .ilike(key, '%' + value + '%')
      .range(from_range, to_range)
      .order(sort);
    return res.data;
  }

  async findOne(collection: string, params: any, columns: string = '*') {
    const res = await this.supabase
      .from(collection)
      .select(columns)
      .match(params);
    return res?.data[0];
  }

  async updateOne(
    collection: string,
    key: string,
    value: string,
    payload: any
  ) {
    return await this.supabase
      .from(collection)
      .update(payload)
      .eq(key, value)
      .select()
      .single();
  }

  async deleteOne(collection: string, key: string, value: string) {
    return await this.supabase.from(collection).delete().eq(key, value);
  }

  async upsert(collection: string, key: string, value: string, payload: any) {
    return await this.supabase.from(collection).upsert(payload).eq(key, value);
  }

  async countDocs(collection: string) {
    const res = await this.supabase
      .from(collection)
      .select('*', { count: 'exact', head: true });

    return res.count;
  }
}
