import { Component, HostBinding, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { App } from '@capacitor/app';
import { environment } from 'src/environments/environment';
import {
  DARK_MODE,
  DARK_MODE_CLASS_NAME,
  OFF,
  ON,
} from './shared/constants/strings';
import { OverlayContainer } from '@angular/cdk/overlay';
import { FormControl } from '@angular/forms';
import { StorageService } from './services/storage/storage.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  @HostBinding() class = '';
  cap = App;
  toggleControl = new FormControl(false);

  constructor(
    private readonly router: Router,
    private readonly zone: NgZone,
    private readonly overlay: OverlayContainer,
    private readonly store: StorageService
  ) {}

  initializeApp() {
    this.cap.addListener('appUrlOpen', (data: { url?: string }) => {
      this.zone.run(() => {
        const slug = data.url?.split(`${environment.redirectUri}://`).pop();
        if (slug) {
          this.router.navigateByUrl(slug);
        }
      });
    });
  }

  setTheme(darkMode: boolean) {
    this.saveTheme(darkMode);
    if (darkMode) {
      this.class = DARK_MODE_CLASS_NAME;
      this.overlay.getContainerElement().classList.add(this.class);
    } else {
      this.class = '';
      this.overlay.getContainerElement().classList.remove(DARK_MODE_CLASS_NAME);
    }
  }

  toggleDarkMode(darkMode: boolean) {
    if (darkMode) {
      this.toggleControl.setValue(true);
    } else {
      this.toggleControl.setValue(false);
    }
  }

  saveTheme(darkMode: boolean) {
    if (darkMode) {
      this.store.setItem(DARK_MODE, ON);
    } else {
      this.store.setItem(DARK_MODE, OFF);
    }
  }
}
