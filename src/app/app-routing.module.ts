import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },

  // AUTH
  {
    path: 'login',
    loadChildren: () =>
      import('./pages/login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'register',
    loadChildren: () =>
      import('./pages/register/register.module').then(
        (m) => m.RegisterPageModule
      ),
  },

  // CUSTOMER
  {
    path: 'list-customers',
    loadChildren: () =>
      import('./pages/customer/list-customers/list-customers.module').then(
        (m) => m.ListCustomersPageModule
      ),
  },
  {
    path: 'add-customer',
    loadChildren: () =>
      import('./pages/customer/add-customer/add-customer.module').then(
        (m) => m.AddCustomerPageModule
      ),
  },
  {
    path: 'edit-customer/:customerUuid',
    loadChildren: () =>
      import('./pages/customer/add-customer/add-customer.module').then(
        (m) => m.AddCustomerPageModule
      ),
  },
  {
    path: 'customer/:customerUuid',
    loadChildren: () =>
      import('./pages/customer/view-customer/view-customer.module').then(
        (m) => m.ViewCustomerPageModule
      ),
  },

  // MEASUREMENTS
  {
    path: 'customer/:customerUuid/add-measurement',
    loadChildren: () =>
      import('./pages/measurement/add-measurement/add-measurement.module').then(
        (m) => m.AddMeasurementPageModule
      ),
  },
  {
    path: 'customer/:customerUuid/list-measurement',
    loadChildren: () =>
      import(
        './pages/measurement/list-measurement/list-measurement.module'
      ).then((m) => m.ListMeasurementPageModule),
  },

  // PURCHASE INVOICE
  {
    path: 'list-purchase-invoice',
    loadChildren: () =>
      import(
        './pages/purchase-invoice/list-purchase-invoices/list-purchase-invoices.module'
      ).then((m) => m.ListPurchaseInvoicesPageModule),
  },
  {
    path: 'add-purchase-invoice/:uuid',
    loadChildren: () =>
      import(
        './pages/purchase-invoice/add-purchase-invoice/add-purchase-invoice.module'
      ).then((m) => m.AddPurchaseInvoicePageModule),
  },
  {
    path: 'add-purchase-invoice',
    loadChildren: () =>
      import(
        './pages/purchase-invoice/add-purchase-invoice/add-purchase-invoice.module'
      ).then((m) => m.AddPurchaseInvoicePageModule),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
