import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListMeasurementPageRoutingModule } from './list-measurement-routing.module';

import { ListMeasurementPage } from './list-measurement.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListMeasurementPageRoutingModule
  ],
  declarations: [ListMeasurementPage]
})
export class ListMeasurementPageModule {}
