import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { CUSTOMER } from '../../../shared/constants/strings';
import { CrudService } from 'src/app/services/crud/crud.service';
import { filter, map } from 'rxjs';

@Component({
  selector: 'app-list-measurement',
  templateUrl: './list-measurement.page.html',
  styleUrls: ['./list-measurement.page.scss'],
})
export class ListMeasurementPage implements OnInit {
  spinner: boolean = true;
  customerUuid: string = '';
  measurement: any = [];
  constructor(
    private readonly route: ActivatedRoute,
    private readonly crudService: CrudService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.customerUuid = this.route.snapshot.params['customerUuid'];
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map((event) => {
          this.loadMeasurements();
          return event;
        })
      )
      .subscribe();
  }

  loadMeasurements() {
    this.crudService
      .findOne('measurement', { customer_uuid: this.customerUuid })
      .then((res: any) => {
        this.measurement = [];
        if (res) {
          this.measurement = JSON.parse(res?.measurements);
        }
        this.spinner = false;
      });
  }
}
