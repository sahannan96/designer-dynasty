import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { CrudService } from 'src/app/services/crud/crud.service';

@Component({
  selector: 'app-add-measurement',
  templateUrl: './add-measurement.page.html',
  styleUrls: ['./add-measurement.page.scss'],
})
export class AddMeasurementPage implements OnInit {
  spinner: boolean = true;
  customerUuid: string = '';
  measurementForm: FormGroup = new FormGroup({
    measurement: new FormArray([]),
  });
  measurementFormControl: FormArray = this.measurements[
    'measurement'
  ] as FormArray;

  get measurements() {
    return this.measurementForm.controls['measurement'] as FormArray;
  }

  constructor(
    private readonly toastController: ToastController,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly crudService: CrudService
  ) {}

  ngOnInit() {
    this.customerUuid = this.route.snapshot.params['customerUuid'];
    this.loadMeasurements();
  }

  addMeasurement() {
    const property = new FormGroup({
      title: new FormControl(),
      value: new FormControl(),
    });
    this.measurements.push(property);
  }

  removeMeasurement(i: number, m: any) {
    this.measurements.removeAt(i);
    console.log(i, m);

    this.crudService.deleteOne('measurement','',m.title)
  }

  loadMeasurements() {
    this.crudService
      .findOne('measurement', { customer_uuid: this.customerUuid })
      .then((res) => {
        res ? this.populateForm(res) : (this.spinner = false);
      });
  }

  populateForm(res) {
    Object.keys(res.measurements).forEach((m) => {
      this.measurements.push(
        new FormGroup({
          title: new FormControl(m),
          value: new FormControl(res.measurements[m]),
        })
      );
    });
    this.spinner = false;
  }

  updateMeasurements() {
    this.spinner = true;
    let measurements = [];
    this.measurements.value.forEach((element) => {
      measurements.push({ key: element.title, value: element.value });
    });
    this.crudService
      .upsert('measurement', 'customr', this.customerUuid, {
        measurements: JSON.stringify(measurements),
        customer_uuid: this.customerUuid,
      })
      .then(() => {
        this.presentToast('Measurements Updated');
        this.spinner = false;
      });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
