import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListPurchaseInvoicesPage } from './list-purchase-invoices.page';

const routes: Routes = [
  {
    path: '',
    component: ListPurchaseInvoicesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListPurchaseInvoicesPageRoutingModule {}
