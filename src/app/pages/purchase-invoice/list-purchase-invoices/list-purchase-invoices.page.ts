import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter, map } from 'rxjs';
import { CrudService } from 'src/app/services/crud/crud.service';

@Component({
  selector: 'app-list-purchase-invoices',
  templateUrl: './list-purchase-invoices.page.html',
  styleUrls: ['./list-purchase-invoices.page.scss'],
})
export class ListPurchaseInvoicesPage implements OnInit {
  spinner: boolean = false;
  invoices: any[] = [];
  constructor(
    private readonly crudService: CrudService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map((event) => {
          this.loadInvoices();
          return event;
        })
      )
      .subscribe();
  }

  loadInvoices() {
    this.crudService
      .list(
        'purchase_invoice',
        '*,items("*")',
        undefined,
        undefined,
        undefined,
        'bill_no'
      )
      .then((res) => {
        this.spinner = false;
        if (res?.length) {
          this.invoices = res;
        }
      });
  }
}
