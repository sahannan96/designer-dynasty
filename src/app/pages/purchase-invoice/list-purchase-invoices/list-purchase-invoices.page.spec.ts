import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ListPurchaseInvoicesPage } from './list-purchase-invoices.page';

describe('ListPurchaseInvoicesPage', () => {
  let component: ListPurchaseInvoicesPage;
  let fixture: ComponentFixture<ListPurchaseInvoicesPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ListPurchaseInvoicesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
