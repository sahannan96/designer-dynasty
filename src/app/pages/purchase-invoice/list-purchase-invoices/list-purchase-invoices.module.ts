import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListPurchaseInvoicesPageRoutingModule } from './list-purchase-invoices-routing.module';

import { ListPurchaseInvoicesPage } from './list-purchase-invoices.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListPurchaseInvoicesPageRoutingModule
  ],
  declarations: [ListPurchaseInvoicesPage]
})
export class ListPurchaseInvoicesPageModule {}
