import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddPurchaseInvoicePage } from './add-purchase-invoice.page';

const routes: Routes = [
  {
    path: '',
    component: AddPurchaseInvoicePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddPurchaseInvoicePageRoutingModule {}
