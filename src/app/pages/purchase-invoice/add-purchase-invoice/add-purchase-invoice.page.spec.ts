import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AddPurchaseInvoicePage } from './add-purchase-invoice.page';

describe('AddPurchaseInvoicePage', () => {
  let component: AddPurchaseInvoicePage;
  let fixture: ComponentFixture<AddPurchaseInvoicePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(AddPurchaseInvoicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
