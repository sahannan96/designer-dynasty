import { Component, OnInit } from '@angular/core';
import { ItemComponent } from '../item/item.component';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AlertController,
  ToastController,
  ModalController,
} from '@ionic/angular';
import { CrudService } from 'src/app/services/crud/crud.service';
import { FormArray, FormControl, FormGroup } from '@angular/forms';
import { Observable, map, startWith } from 'rxjs';

@Component({
  selector: 'app-add-purchase-invoice',
  templateUrl: './add-purchase-invoice.page.html',
  styleUrls: ['./add-purchase-invoice.page.scss'],
})
export class AddPurchaseInvoicePage implements OnInit {
  // purchase_invoice: any = {};
  uuid: string;
  spinner: boolean = false;
  purchaseInvoiceForm = new FormGroup({
    bill_no: new FormControl(),
    date: new FormControl(new Date()),
    party_name: new FormControl(),
    party_phone: new FormControl(),
    items: new FormArray([]),
    notes: new FormControl(),
    total: new FormControl(),
  });
  calculateTotal: Observable<any>;

  itemsControl: FormArray = this.purchaseInvoiceForm.get('items') as FormArray;

  constructor(
    private readonly crudService: CrudService,
    private readonly route: ActivatedRoute,
    private readonly alertController: AlertController,
    private readonly toastController: ToastController,
    private readonly router: Router,
    private readonly modalController: ModalController
  ) {}

  get f() {
    return this.purchaseInvoiceForm.controls;
  }

  ngOnInit() {
    this.spinner = true;
    this.calculateTotal = this.itemsControl.valueChanges
      .pipe(startWith(this.itemsControl.value))
      .pipe(
        map((items) => {
          let total: number = 0;
          items.forEach((item) => {
            total += item.total;
          });
          return total;
        })
      );
    this.uuid = this.route.snapshot.params['uuid'];
    if (this.uuid) {
      this.loadInvoice();
    } else {
      this.getBillNo();
    }
    this.calculateTotal.subscribe((res) => {
      this.f.total.setValue(res);
      // this.invoice_total = res;
    });
  }

  getBillNo() {
    this.crudService
      .findOne(
        'naming_series',
        {
          invoice_type: 'purchase_invoice',
        },
        'value'
      )
      .then((res: any) => {
        if (!res) {
          // TO BE DONE DURING SETUP
          this.crudService
            .create('naming_series', {
              invoice_type: 'purchase_invoice',
              value: '001',
            })
            .then(() => {
              this.getBillNo();
            });
        }
        this.f.bill_no.setValue(res.value);
        this.spinner = false;
      });
  }

  loadInvoice() {
    this.crudService
      .findOne('purchase_invoice', { uuid: this.uuid }, '*,items("*")')
      .then((res) => {
        console.log(res);

        // this.purchase_invoice = res;
        this.spinner = false;
      });
  }

  whatsappShare() {
    const message = 'Good Message';
    location.href =
      'https://wa.me/' +
      this.f.party_phone +
      '?text=' +
      encodeURIComponent(message);
  }

  async updateItem(item?: any) {
    const modal = await this.modalController.create({
      component: ItemComponent,
      componentProps: { item: item?.value },
    });
    modal.present();

    const { data, role } = await modal.onWillDismiss();
    if (role === 'confirm') {
      const itemForm = new FormGroup({
        uuid: new FormControl(data.uuid),
        name: new FormControl(data.name),
        quantity: new FormControl(data.quantity),
        rate: new FormControl(data.rate),
        unit: new FormControl(data.unit),
        total: new FormControl(data.total),
      });
      if (
        this.itemsControl.value.find(
          (singleItemControl: any) => singleItemControl.uuid === data.uuid
        )
      ) {
        let index = this.itemsControl.value.findIndex(
          (singleItemControl: any) => singleItemControl.uuid === data.uuid
        );
        this.itemsControl.removeAt(index);
        this.itemsControl.insert(index, itemForm);
      } else {
        this.itemsControl.push(itemForm);
      }
      console.log(this.itemsControl.value);
    }
  }

  saveInvoice() {
    this.crudService.create('items',this.purchaseInvoiceForm.value.items).then(res=>{
      console.log(res);
      
    })
    console.log(this.purchaseInvoiceForm.value);
  }

  confirmDeleteCustomer() {
    this.presentAlert(
      `Are you sure you want to delete customer ${this.f.party_name}?`
    );
  }

  deleteCustomer() {
    this.crudService
      .deleteOne('purchase_invoice', 'uuid', this.uuid)
      .then(() => {
        this.presentToast('Customer Deleted');
        this.router.navigate(['/list-purchase-invoice']);
      })
      .catch((err) => {
        this.presentToast(err);
      })
      .finally(() => {
        this.spinner = false;
      });
  }

  async presentAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: msg,
      cssClass: 'custom-alert',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'alert-button-cancel',
        },
        {
          text: 'Confirm',
          role: 'confirm',
          cssClass: 'alert-button-confirm',
          handler: () => {
            this.spinner = true;
            this.deleteCustomer();
          },
        },
      ],
    });
    await alert.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
