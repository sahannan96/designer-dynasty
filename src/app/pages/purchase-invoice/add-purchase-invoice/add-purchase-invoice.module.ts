import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddPurchaseInvoicePageRoutingModule } from './add-purchase-invoice-routing.module';

import { AddPurchaseInvoicePage } from './add-purchase-invoice.page';
import { ItemComponent } from '../item/item.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AddPurchaseInvoicePageRoutingModule,
  ],
  declarations: [AddPurchaseInvoicePage, ItemComponent],
})
export class AddPurchaseInvoicePageModule {}
