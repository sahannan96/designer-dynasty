import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import {
  Observable,
  distinctUntilChanged,
  from,
  startWith,
  switchMap,
} from 'rxjs';
import { CrudService } from 'src/app/services/crud/crud.service';
import { ITEM_UNIT } from 'src/app/shared/constants/app-string';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss'],
})
export class ItemComponent implements OnInit {
  @Input() item: any;
  units: string[];
  itemForm = new FormGroup({
    uuid: new FormControl(Math.floor(Math.random() * 1000)),
    name: new FormControl('', [Validators.required]),
    quantity: new FormControl(null, [Validators.required]),
    rate: new FormControl(null, [Validators.required]),
    unit: new FormControl(null, [Validators.required]),
    total: new FormControl(null, [Validators.required]),
  });
  filteredItemList: Observable<any[]>;
  showList: boolean = true;
  invalidForm: boolean = true;

  get f() {
    return this.itemForm.controls;
  }

  constructor(
    private readonly modalController: ModalController,
    private readonly crudService: CrudService,
    private readonly changeDetectorRef: ChangeDetectorRef
  ) {}

  ngOnInit() {
    this.units = Object.values(ITEM_UNIT);
    if (this.item) {
      this.populateForm();
    }
    this.filteredItemList = this.f.name.valueChanges.pipe(
      startWith(''),
      distinctUntilChanged(),
      switchMap((value) => {
        return from(
          this.crudService.list('items', undefined, { name: value }, 3)
        );
      })
    );
  }

  ngAfterViewChecked(): void {
    this.changeDetectorRef.detectChanges();
  }

  populateForm() {
    this.f.uuid.setValue(this.item.uuid);
    this.f.name.setValue(this.item.name);
    this.f.quantity.setValue(this.item.quantity);
    this.f.rate.setValue(this.item.rate);
    this.f.unit.setValue(this.item.unit);
    this.f.total.setValue(this.item.total);
  }

  calculateTotal() {
    this.f.total.setValue(this.f.quantity.value * this.f.rate.value);
    return this.f.total.value;
  }

  cancel() {
    return this.modalController.dismiss(null, 'cancel');
  }

  confirm() {
    return this.modalController.dismiss(this.itemForm.value, 'confirm');
  }

  selectItem(item: any) {
    console.log(item);

    this.f.name.setValue(item.name);
    this.f.rate.setValue(item.rate);
    this.f.quantity.setValidators([Validators.max(item.quantity)]);

    this.showList = false;
  }
}
