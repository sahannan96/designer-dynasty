import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ViewPurchaseInvoicePage } from './view-purchase-invoice.page';

describe('ViewPurchaseInvoicePage', () => {
  let component: ViewPurchaseInvoicePage;
  let fixture: ComponentFixture<ViewPurchaseInvoicePage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ViewPurchaseInvoicePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
