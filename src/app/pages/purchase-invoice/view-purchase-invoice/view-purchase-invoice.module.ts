import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewPurchaseInvoicePageRoutingModule } from './view-purchase-invoice-routing.module';

import { ViewPurchaseInvoicePage } from './view-purchase-invoice.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ViewPurchaseInvoicePageRoutingModule,
  ],
  declarations: [ViewPurchaseInvoicePage],
})
export class ViewPurchaseInvoicePageModule {}
