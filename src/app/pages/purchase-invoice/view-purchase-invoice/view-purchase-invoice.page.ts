import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AlertController,
  ModalController,
  ToastController,
} from '@ionic/angular';
import { CrudService } from 'src/app/services/crud/crud.service';
import { ItemComponent } from '../item/item.component';

@Component({
  selector: 'app-view-purchase-invoice',
  templateUrl: './view-purchase-invoice.page.html',
  styleUrls: ['./view-purchase-invoice.page.scss'],
})
export class ViewPurchaseInvoicePage implements OnInit {
  purchase_invoice: any = {};
  spinner: boolean = false;
  edit: boolean = false;
  constructor(
    private readonly crudService: CrudService,
    private readonly route: ActivatedRoute,
    private readonly alertController: AlertController,
    private readonly toastController: ToastController,
    private readonly router: Router,
    private readonly modalController: ModalController
  ) {}

  ngOnInit() {
    this.spinner = true;
    this.purchase_invoice.uuid = this.route.snapshot.params['uuid'];
    this.purchase_invoice.uuid ? this.loadInvoice() : (this.spinner = false);
  }

  loadInvoice() {
    this.crudService
      .findOne(
        'purchase_invoice',
        { uuid: this.purchase_invoice.uuid },
        '*,items("*")'
      )
      .then((res) => {
        this.purchase_invoice = res;
        this.spinner = false;
        // this.authService
        //   .updateUser({
        //     name: 'Abdul Hannan Shaikh',
        //     phone: 8286547834,
        //   })
        //   .then((res) => {
        //     console.log(res);
        //   });
      });
  }

  calculateTotal() {
    let total = 0;
    this.purchase_invoice.items?.forEach((item) => {
      total = total + item.rate * item.quantity;
    });
    return total;
  }

  whatsappShare() {
    const message = 'Good Message';
    location.href =
      'https://wa.me/' +
      this.purchase_invoice.party_phone +
      '?text=' +
      encodeURIComponent(message);
  }

  addItem() {}

  async editItem() {
    const modal = await this.modalController.create({
      component: ItemComponent,
    });
    modal.present();

    const { data, role } = await modal.onWillDismiss();

    if (role === 'confirm') {
      console.log(data);

      // this.message = `Hello, ${data}!`;
    }
  }

  updateInvoice() {}

  confirmDeleteCustomer() {
    this.presentAlert(
      `Are you sure you want to delete customer ${this.purchase_invoice.name}?`
    );
  }

  deleteCustomer() {
    this.crudService
      .deleteOne('purchase_invoice', 'uuid', this.purchase_invoice.uuid)
      .then(() => {
        this.presentToast('Customer Deleted');
        this.router.navigate(['/list-purchase-invoice']);
      })
      .catch((err) => {
        this.presentToast(err);
      })
      .finally(() => {
        this.spinner = false;
      });
  }

  async presentAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: msg,
      cssClass: 'custom-alert',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'alert-button-cancel',
        },
        {
          text: 'Confirm',
          role: 'confirm',
          cssClass: 'alert-button-confirm',
          handler: () => {
            this.spinner = true;
            this.deleteCustomer();
          },
        },
      ],
    });
    await alert.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
