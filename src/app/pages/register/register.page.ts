import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import {
  AlertController,
  LoadingController,
  NavController,
} from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup = new FormGroup({
    email: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });
  constructor(
    private loadingController: LoadingController,
    private authService: AuthService,
    private router: Router,
    private alertController: AlertController,
    private navCtrl: NavController
  ) {}

  ngOnInit() {}

  async register() {
    const loading = await this.loadingController.create();
    await loading.present();

    this.authService
      .signUp(this.registerForm.getRawValue())
      .then(async (data) => {
        await loading.dismiss();
        if (data.error) {
          this.showAlert('Registration failed', data.error.message);
        } else {
          this.showAlert('Signup success', 'Please confirm your email now!');
          this.navCtrl.navigateBack('');
        }
      });
  }

  async showAlert(title, msg) {
    const alert = await this.alertController.create({
      header: title,
      message: msg,
      buttons: ['OK'],
    });
    await alert.present();
  }
}
