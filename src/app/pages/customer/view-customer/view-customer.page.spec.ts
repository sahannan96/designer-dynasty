import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ViewCustomerPage } from './view-customer.page';

describe('ViewCustomerPage', () => {
  let component: ViewCustomerPage;
  let fixture: ComponentFixture<ViewCustomerPage>;

  beforeEach(async(() => {
    fixture = TestBed.createComponent(ViewCustomerPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
