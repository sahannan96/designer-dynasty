import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { CrudService } from 'src/app/services/crud/crud.service';
import { Customer } from 'src/app/shared/interfaces/list-data-interface';

@Component({
  selector: 'app-view-customer',
  templateUrl: './view-customer.page.html',
  styleUrls: ['./view-customer.page.scss'],
})
export class ViewCustomerPage implements OnInit {
  customer: Customer = {};
  spinner: boolean = false;
  constructor(
    private readonly crudService: CrudService,
    private readonly route: ActivatedRoute,
    private readonly alertController: AlertController,
    private readonly toastController: ToastController,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.spinner = true;
    this.customer.uuid = this.route.snapshot.params['customerUuid'];
    this.customer.uuid ? this.loadCustomer() : (this.spinner = false);
  }

  loadCustomer() {
    this.crudService
      .findOne('customers', { uuid: this.customer.uuid })
      .then((res) => {
        this.populateForm(res);
      });
  }

  populateForm(res: any) {
    this.customer.name = res.name;
    this.customer.phone = res.phone;
    this.customer.email = res.email;
    this.customer.address = res.address;
    this.spinner = false;
  }

  confirmDeleteCustomer() {
    this.presentAlert(
      `Are you sure you want to delete customer ${this.customer.name}?`
    );
  }

  deleteCustomer() {
    this.crudService
      .deleteOne('customers', 'uuid', this.customer.uuid)
      .then(() => {
        this.presentToast('Customer Deleted');
        this.router.navigate(['/list-customers']);
      })
      .catch((err) => {
        this.presentToast(err);
      })
      .finally(() => {
        this.spinner = false;
      });
  }

  async presentAlert(msg: string) {
    const alert = await this.alertController.create({
      header: 'Alert',
      message: msg,
      cssClass: 'custom-alert',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'alert-button-cancel',
        },
        {
          text: 'Confirm',
          role: 'confirm',
          cssClass: 'alert-button-confirm',
          handler: () => {
            this.spinner = true;
            this.deleteCustomer();
          },
        },
      ],
    });
    await alert.present();
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
