import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { filter, map } from 'rxjs';
import { CrudService } from 'src/app/services/crud/crud.service';

@Component({
  selector: 'app-list-customers',
  templateUrl: './list-customers.page.html',
  styleUrls: ['./list-customers.page.scss'],
})
export class ListCustomersPage implements OnInit {
  spinner: boolean = false;
  customers: any[] = [];
  filteredList: any[] = [];
  constructor(
    private readonly crudService: CrudService,
    private readonly router: Router
  ) {}

  ngOnInit() {
    this.router.events
      .pipe(
        filter((event) => event instanceof NavigationEnd),
        map((event) => {
          this.loadCustomers();
          return event;
        })
      )
      .subscribe();
  }

  loadCustomers() {
    this.crudService
      .list(
        'customers',
        'uuid,name,phone',
        undefined,
        undefined,
        undefined,
        'name'
      )
      .then((res) => {
        this.spinner = false;
        if (res?.length) {
          this.customers = this.filteredList = res;
        }
      });
  }

  onSearchChange(event) {
    if (event.detail.value) {
      this.filteredList = this.customers.filter((item) => {
        return (
          new RegExp(event.detail.value, 'gi').test(item['name']) ||
          event.detail.value == ''
        );
      });
    } else {
      this.filteredList = this.customers;
    }
  }
}
