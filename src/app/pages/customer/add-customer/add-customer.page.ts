import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Customer } from '../../../shared/interfaces/list-data-interface';
import { ToastController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudService } from 'src/app/services/crud/crud.service';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.page.html',
  styleUrls: ['./add-customer.page.scss'],
})
export class AddCustomerPage implements OnInit {
  spinner: boolean = false;
  customer: Customer = {};
  customerForm = new FormGroup({
    name: new FormControl('', Validators.required),
    phone: new FormControl(null, [
      Validators.required,
      Validators.pattern('^[0-9]*$'),
      Validators.minLength(10),
      Validators.maxLength(10),
    ]),
    email: new FormControl('', Validators.email),
    address: new FormControl(''),
  });

  get f() {
    return this.customerForm.controls;
  }

  constructor(
    private readonly toastController: ToastController,
    private readonly router: Router,
    private readonly route: ActivatedRoute,
    private readonly crudService: CrudService
  ) {}

  ngOnInit() {
    this.spinner = true;
    this.customer.uuid = this.route.snapshot.params['customerUuid'];
    this.customer.uuid ? this.loadCustomer() : (this.spinner = false);
  }

  addCustomer() {
    this.spinner = true;
    const customer = {} as Customer;
    customer.name =
      this.f.name.value.charAt(0).toUpperCase() + this.f.name.value.slice(1);
    customer.phone = parseInt(this.f.phone.value);
    customer.email = this.f.email.value;
    customer.address = this.f.address.value;

    this.crudService
      .create('customers', customer)
      .then(() => {
        this.presentToast('Customer Added');
        this.router.navigate(['/list-customers']);
      })
      .catch((err) => this.presentToast(err))
      .finally(() => {
        this.spinner = false;
      });
  }

  loadCustomer() {
    this.crudService
      .findOne('customers', { uuid: this.customer.uuid })
      .then((res: any) => {
        console.log(res);

        this.customer = res;
        this.customer.uuid = res.uuid;
        this.populateForm();
      })
      .catch((err) => this.presentToast(err))
      .finally(() => {
        this.spinner = false;
      });
  }

  populateForm() {
    this.f.name.setValue(this.customer.name);
    this.f.phone.setValue(this.customer.phone);
    this.f.email.setValue(this.customer.email);
    this.f.address.setValue(this.customer.address);
    this.spinner = false;
  }

  updateCustomer() {
    this.spinner = true;
    const customer = {} as Customer;
    customer.name =
      this.f.name.value.charAt(0).toUpperCase() + this.f.name.value.slice(1);
    customer.phone = parseInt(this.f.phone.value);
    customer.email = this.f.email.value;
    customer.address = this.f.address.value;
    this.crudService
      .updateOne('customers', 'uuid', this.customer.uuid, customer)
      .then((res) => {
        this.customer = res.data;
        this.populateForm();
        this.presentToast('Customer Updated');
      })
      .catch((err) => this.presentToast(err))
      .finally(() => {
        this.spinner = false;
      });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 2000,
    });
    toast.present();
  }
}
