export const DARK_MODE = 'dark_mode';
export const ON = 'on';
export const OFF = 'off';
export const ADMINISTRATOR = 'administrator';
export const DARK_MODE_CLASS_NAME = 'darkMode';

export const CUSTOMER = 'customer';
export const ORDERS = 'orders';
export const USERS = 'users';
